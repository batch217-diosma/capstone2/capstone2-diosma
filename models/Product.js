const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required : [true, "Course Name is Required!"]
	},
	description: {
		type: String,
		required: [true, "Description is Required!"]
	},
	category: {
		type: String,
		required: [true, "Category is Required!"]
	},
	price: {
		type: Number,
		required: [true, "Price is Required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders : [{
		orderId : {
			type : String,
			required : [true, "Order ID is required"]
		}
	}]
})


module.exports = mongoose.model("Product", productSchema);