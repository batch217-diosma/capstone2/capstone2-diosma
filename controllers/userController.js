const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/Product.js");

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10),

	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return "New User Resitered"; //<==
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return  {access : auth.createAccessToken(result)}
			}
		}
	})
}

module.exports.checkout = async (data) => {
	
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push({
		productName: [data.product[0].productId]
		});

		return user.save().then((user, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})

	})

	let isOrderUpdated = await Product.findById(data.productId).then(product => {
		product.orders.push({orderId : [data.product[0].productId]});

		return product.save().then((order, error) => {
			if(error){
				return false
			}else{
				return true;
			}
		})
	})

	/*if(isUserUpdated && isOrderUpdated){
		//isUserUpdated = true
		//isCourseUpdatted = true
		//final output = TRUE
		return true;
	}else{
		//If one of these: isUserUpdated or isCourseUpdated is false
		//Output will be false

		//If both isUserUpdated and isCourseUpdated
		//Output will be a concrete FALSE
		return false;
	}*/
}


module.exports.userDetails = (userId) => {
	return User.findById(userId).then(result => {
		return result
	})
}
