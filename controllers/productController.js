




const Product = require("../models/Product")

module.exports.addProduct = (data) => {
	console.log(data.isAdmin)

	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			category: data.product.category,
			price: data.product.price
		})

		return newProduct.save().then((newProduct, error) => {
			if(error){
				return error
			}

			return newProduct 
		})
	} 

	let message = Promise.resolve('User must be ADMIN to access this.')

	return message.then((value) => {
		return value
	})
}

module.exports.getAllActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}

module.exports.getProduct = (productId) => {
	return Product.findById(productId).then(result => {
		return result
	})
}


module.exports.updateProduct = (newData,productId) => {
	console.log(newData.isAdmin)

	if(newData.isAdmin){
	return Product.findByIdAndUpdate(productId,{
		price: newData.product.price,
			category: newData.product.category,
			name: newData.product.name,
			description: newData.product.description
	})
	.then((updatedProduct, error) => {
		if(error){
			return false
		}

		return "Product Updated" // <===
	})
}
let message = Promise.resolve('User must be ADMIN to access this.')

	return message.then((value) => {
		return value
	})
}

module.exports.archiveProduct = (newData,productId) => {
	console.log(newData.isAdmin)

	if(newData.isAdmin){
	return Product.findByIdAndUpdate(productId,{
		isActive: false
	})
	.then((archivedProduct, error) => {
		if(error){
			return false
		}

		return "Product Archived" // <===
	})
}
let message = Promise.resolve('User must be ADMIN to access this.')

	return message.then((value) => {
		return value
	})
}



