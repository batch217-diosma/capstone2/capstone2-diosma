const express = require("express");
const router = express.Router();
const User = require("../models/User.js");
const userController = require("../controllers/userController.js");
const auth = require("../auth.js")

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productName: [req.body.productName],
		/*quantity: .quantity
		totalAmount: result.price*/
	}

	userController.checkout(data).then(resultFromController => res.send(resultFromController));
})


router.get("/userDetails/:userId", (req, res) => {
	userController.userDetails(req.params.userId).then(resultFromController => {
		res.send(resultFromController)
	})
})



module.exports = router;