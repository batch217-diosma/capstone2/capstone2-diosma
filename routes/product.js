const express = require("express");
const router = express.Router();
const Product = require("../models/Product.js");
const productController = require("../controllers/productController.js");
const auth = require("../auth.js")

router.post("/products", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => {
		res.send(resultFromController)
	})
})

router.get("/products", (req, res) => {
	productController.getAllActiveProducts().then(resultFromController => {
		res.send(resultFromController)
	})
})

router.get("/:productId", (req, res) => {
	productController.getProduct(req.params.productId).then(resultFromController => {
		res.send(resultFromController)
	})
})


router.patch("/:productId/update", auth.verify, (req, res) => {
	const newData = {
		product:  (req.body ),
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(newData, req.params.productId).then(resultFromController => {
		res.send(resultFromController)
	})
})




router.patch("/:productId/archive", auth.verify, (req, res) => {
	const newData = {
		
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(newData,req.params.productId).then(resultFromController => {
		res.send(resultFromController)
	})
})


module.exports = router
